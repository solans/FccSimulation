//Dear emacs this is -*-c++-*-
/************************************************
 * CaloConcept.h
 * Calorimeter concept for Geant4 simulation
 * author: Carlos.Solans@cern.ch
 * date: November 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#ifndef CaloConcept_h
#define CaloConcept_h 1

#include "globals.hh"
#include <vector>
#include "G4ThreeVector.hh"

#include "G4Element.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4Step.hh"

class CaloConcept{

public:
  
  //Constructor
  CaloConcept();
  
  //Destructor
  ~CaloConcept();

  //! Get cell bin from a position
  G4int GetCellId(const G4Step * );

  //! Get the cell phi given cell id
  G4double GetCellPhi(G4int cellid);

  //! Get the cell rho given cell id
  G4double GetCellRho(G4int cellid);

  //! Get the cell height (in cylindrical coordinates) given a cell id
  G4double GetCellZ(G4int cellid);
  
  //! Get calorimeter inner radius
  G4double GetRmin();

  //! Get calorimeter outer radius
  G4double GetRmax();

  //! Get calorimeter min phi
  G4double GetPhiMin();

  //! Get calorimeter max phi
  G4double GetPhiMax();
  
  //! Get calorimeter height (in cylindrical coordinates)
  G4double GetHalfZ();

  //! Get calorimeter height (in cylindrical coordinates)
  G4double GetDz();

  //! Get calorimeter minimum Z (in cylindrical coordinates)
  G4double GetZmin();

  //! Get calorimeter maximum Z (in cylindrical coordinates)
  G4double GetZmax();

  //! Get number of phi bins
  G4int GetNumPhiBins();

  //! Get Phi for a given bin
  G4double GetPhiBin(G4int bin);
  
  //! Get number of radial layers 
  G4int GetNumLayers();
  
  //! Get tile type
  G4int GetNumTileTypes();

  //! Get height of a given layer
  G4double GetLayerHeight(G4int layer);
  
  //! Get the azimutal size of each layer
  G4double GetPhiSize();
  
  //! Get number of tiles in beam line given a layer
  G4int GetNumRows(G4int layer);

  //! Get width of tile given a row and layer
  G4double GetRowWidth(G4int row, G4int layer);

  //! Get Z of tile given a row and layer
  G4double GetRowZ(G4int row, G4int layer);

  //! Is the tile active given cell id
  G4bool IsCellActive(G4int cellid);

  //! Get the total number of cells
  G4int GetNumCells();
  
  //! Get the cell size in phi
  G4double GetCellDeltaPhi(G4int cellid);
  
  //! Get the cell size in rho
  G4double GetCellDeltaRho(G4int cellid);
  
  //! Get the cell size in z
  G4double GetCellDeltaZ(G4int cellid);

  //! Build the calorimeter
  void BuildAndPlace(G4LogicalVolume * world);

  //! Get active volumes
  std::vector<G4LogicalVolume*> GetActiveVolumes();

  //! Birk's law
  G4double BirkLaw(const G4Step * aStep) const;

private:
  
  G4Material* Air;
  G4Material* Polystyrene;
  G4Material* Steel;
  G4Material* Lead;

  G4Element* H;
  G4Element* C;
  G4Element* N;
  G4Element* O;
  G4Element* Pb;
  G4Element* Fe;
  G4Element* W;

  G4double rmin;
  G4double rmax;
  G4double zmin;
  G4double zmax;
  G4double dz;
  G4double phimin;
  G4double phimax;
  G4int    num_modules;
  G4double dphi;
  G4double drho; 
  G4int    num_layers;
  G4int    num_layer_types;
  G4int    tile_types;
  G4double epsilon;
  G4double spacing;

  std::vector<G4Material*>           tile_material;
  std::vector<G4double>              tile_width;
  std::vector<G4bool>                tile_active;
  
  std::vector<G4float>               modules_bin;
  std::vector<G4float>               modules_phi;

  std::vector<G4float>               layers_bin;
  std::vector<G4float>               layers_rho;
  std::vector<G4int>                 layers_type;

  std::vector<std::vector<G4float> > rows_bin;
  std::vector<std::vector<G4float> > rows_z;
  std::vector<std::vector<G4int> >   rows_type;
  
  std::vector<G4LogicalVolume*>      active_volumes;
  

  
};

#endif
