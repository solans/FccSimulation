//Dear emacs this is -*-c++-*-
/************************************************
 * DetectorConstructor.h
 * Geant4 detector constructor
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#ifndef DetectorConstructor_h
#define DetectorConstructor_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4Version.hh"

class G4VPhysicalVolume;
class G4Material;
class G4Element;
class CaloConcept;

class DetectorConstructor : public G4VUserDetectorConstruction{

public:
  
  //Constructor
  DetectorConstructor();
  
  //Destructor
  ~DetectorConstructor();
  
  //specific implementation
  G4VPhysicalVolume* Construct();

#if G4VERSION_NUMBER >= 1000
  //construct non-shared objects
  void ConstructSDandField();
#endif   

private:
  
  G4double expHall_x;
  G4double expHall_y;
  G4double expHall_z;
  
  G4Material* Air;
  G4Element* N;
  G4Element* O;

  CaloConcept* caloConcept;
};

#endif

