/************************************************
 * ParticleGun.h
 * Geant4 event generator
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#ifndef ParticleGun_h
#define ParticleGun_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Event;
class G4ParticleGun;

class ParticleGun : public G4VUserPrimaryGeneratorAction{
public:
  
  //Constructor
  ParticleGun();
  
  //Destructor
  ~ParticleGun();
  
  //Callback method to get next event
  virtual void GeneratePrimaries(G4Event*);

  //Configure the gun
  void Configure(const char * particle, float energyGeV, const G4ThreeVector & dir);
  
private:

  //Particle gun
  G4ParticleGun* particleGun;  
  
};

#endif
