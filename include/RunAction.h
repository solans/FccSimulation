#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;

class RunAction : public G4UserRunAction{
public:
  
  //Constructor
  RunAction(G4String outputfile="output.root");
  
  //Destructor
  ~RunAction();

  //Callback at the beginning of run
  virtual void BeginOfRunAction(const G4Run*);
  
  //Callback at the end of run
  virtual void EndOfRunAction (const G4Run*);

  //Ouput file
  G4String outfile;
};

#endif
