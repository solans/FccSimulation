//Dear emacs this is -*-c++-*-
/************************************************
 * SensitiveDetector.h
 * Geant4 sensitive detector
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "SimpleHit.h"
#include "CaloConcept.h"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class SensitiveDetector : public G4VSensitiveDetector{

public:
  
  //Constructor
  SensitiveDetector(G4String name, CaloConcept * cc);
  
  //Destructor
  ~SensitiveDetector();
  
  //Invoked at the beginning of each event
  /*virtual*/ void Initialize(G4HCofThisEvent*);

  //Invoked at every step in the volume of the detector
  /*virtual*/ G4bool ProcessHits(G4Step*,G4TouchableHistory*);
  
  //Invoked at the end of the event
  /*virtual*/ void EndOfEvent(G4HCofThisEvent*);
  
  /*
  void clear();
  void DrawAll();
  void PrintAll();
  */

private:
  
  SimpleHitsCollection * hitsCollection;
  CaloConcept * caloConcept;

};

#endif
