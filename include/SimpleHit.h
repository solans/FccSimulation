//Dear emacs this is -*-c++-*-
/************************************************
 * SimpleHit.h
 * Geant4 Hit 
 * author: Carlos.Solans@cern.ch
 * date: November 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#ifndef SimpleHit_h
#define SimpleHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4Types.hh"
#include "G4ThreeVector.hh"

class SimpleHit : public G4VHit{
public:
  
  //Constructor
  SimpleHit();
  
  //Copy constructor
  //SimpleHit(const SimpleHit &);

  //Destructor
  virtual ~SimpleHit();
  
  void AddEnergy(G4double);

  G4double GetEnergy();
  G4int GetId();
  const G4ThreeVector & GetPosition();
  
  void SetEnergy(G4double);
  void SetId(G4int);
  void SetPosition(const G4ThreeVector&);

  //Other stuff
  //const SimpleHit& operator=(const SimpleHit&);
  //int operator==(const SimpleHit&) const;
  inline void*operator new(size_t);
  inline void operator delete(void*);
  virtual void Draw(); 
  virtual void Print();

private:
  
  //Local variables
  G4double m_e;
  //G4double m_p1;
  //G4double m_p2;
  //G4double m_p3;
  G4ThreeVector m_pos;
  G4int m_id;

};

typedef G4THitsCollection<SimpleHit> SimpleHitsCollection;

extern G4Allocator<SimpleHit> SimpleHitAllocator;

inline void* SimpleHit::operator new(size_t){
  return (void*) SimpleHitAllocator.MallocSingle();
}

inline void SimpleHit::operator delete(void* hit){
  SimpleHitAllocator.FreeSingle((SimpleHit*) hit);
}

#endif
