#ifndef StackingAction_h
#define StackingAction_h 1

#include "G4UserStackingAction.hh"
#include "globals.hh"

class G4Track;

class StackingAction : public G4UserStackingAction{
public:
  
  //Constructor
  StackingAction();
  
  //Destructor
  virtual ~StackingAction();
      
  //Callback each time a new track is created
  virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);      
  
};

#endif
