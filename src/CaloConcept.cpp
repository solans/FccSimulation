#include "CaloConcept.h"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include <algorithm>
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include "templates.hh"

bool cmpfunc(const G4float &a, const G4float &b){
  G4cout << "a:" << a << " b:" << b << G4endl;
  return !((b-a)<0.001);
}

CaloConcept::CaloConcept(){
  
  G4double a, z, density;
  G4int nel, natom;

  H  = new G4Element("Hidrogen", "H",  z=1.,  a=  1.01*g/mole);  
  C  = new G4Element("Carbon",   "C",  z=6.,  a= 16.00*g/mole);  
  N  = new G4Element("Nitrogen", "N",  z=7.,  a= 14.01*g/mole);
  O  = new G4Element("Oxygen",   "O",  z=8.,  a= 16.00*g/mole);  
  Pb = new G4Element("Lead",     "Pb", z=82., a=207.19*g/mole);
  Fe = new G4Element("Iron",     "Fe", z=26., a= 55.85*g/mole);
  W  = new G4Element("Tungstenm","W",  z=74., a=183.85*g/mole);

  Air = new G4Material("Air", density=1.29*mg/cm3, nel=2);
  Air->AddElement(N, 70.*perCent);
  Air->AddElement(O, 30.*perCent);

  Polystyrene = new G4Material("Polystyrene", density=1.032*g/cm3, nel=2);
  Polystyrene->AddElement(C, natom=8);
  Polystyrene->AddElement(H, natom=8);

  Lead = new G4Material("Lead",density = 11.35*g/cm3, nel=1);
  Lead->AddElement(Pb, 100.*perCent);

  Steel = new G4Material("Steel",density = 7.85*g/cm3, nel=1);
  Steel->AddElement(Fe, 100.*perCent);



  //This is based on a tile calorimeter concept
  //Using cylindrical coordinates:
  //z   : parallel to beam pipe
  //phi : azimuthal angle
  //rho : perpendicular to beam pipe
  //
  //Definitions
  //Layer : the height of a tile in rho direction
  //Module: the size of a tile in phi
  //Row   : the width/position of a tile in z
  //
  rmin        =  1.*m;
  rmax        = 10.*m;
  zmin        = -1.*m;
  zmax        = +5.*m;
  dz          = zmax-zmin;
  phimin      = -180.*deg; //Cannot specify negative phi: G4Tubs does not build in in 4.10
  phimax      =  180.*deg;
  num_modules = 128;
  dphi        = (phimax-phimin)/num_modules;
  drho        = 10.*cm; //tile height
  num_layers  = (rmax - rmin)/drho; //the number of layers is fixed by the tile height
  epsilon     = 1E-7;
  spacing     = 0.001;

  tile_types  = 4;
  
  //Master plate
  tile_material.push_back(Steel);
  tile_width.push_back(5.*mm);
  tile_active.push_back(false);
  //Spacer plate
  tile_material.push_back(Steel);
  tile_width.push_back(4.*mm);
  tile_active.push_back(false);
  //Air
  tile_material.push_back(Air);
  tile_width.push_back(0.5*mm);
  tile_active.push_back(false);
  //Scintillator
  tile_material.push_back(Polystyrene);
  tile_width.push_back(3.*mm);
  tile_active.push_back(true);

  std::vector<G4int> tile_sequence[2];
  tile_sequence[0].push_back(0); // Master
  tile_sequence[0].push_back(1); // Spacer
  tile_sequence[0].push_back(0); // Master
  tile_sequence[0].push_back(2); // Air
  tile_sequence[0].push_back(3); // Scintillator
  tile_sequence[0].push_back(2); // Air

  tile_sequence[1].push_back(0); // Master
  tile_sequence[1].push_back(2); // Air
  tile_sequence[1].push_back(3); // Scintillator
  tile_sequence[1].push_back(2); // Air
  tile_sequence[1].push_back(0); // Master
  tile_sequence[1].push_back(1); // Spacer

  //find out how many repetitions we need to fit
  float sequence_width=0.;
  for(unsigned int i=0; i<tile_sequence[0].size(); i++){
	sequence_width+=tile_width.at(tile_sequence[0].at(i));
  }
  G4double num_reps = (int)(dz/sequence_width);

  //Make adjustments to dimensions
  
  //Resize Z to num_reps periods plus a master plate at the end
  G4double ddz = sequence_width * num_reps + tile_width.at(tile_sequence[0].at(0)) - dz;
  zmin -= ddz/2.;
  zmax += ddz/2.;
  dz = zmax-zmin;

  //Resize the calorimeter to add a front-plate in front of each submodule
  G4double drho_frontplate = 10.*mm; 
  rmax += drho_frontplate;

  //there is alternation between spacer and active material 
  //for the first sampling as a function of layer
  num_layer_types = 2;


  //build maps for later use
  //Each value is the lower edge of the bin. 
  //Bin 0 is underflow
  //Bin N is overflow

  //Phi
  //modules_phi: first value of phi in each module
  G4float phi=phimin;
  modules_bin.push_back(FLT_MIN);
  modules_phi.push_back(FLT_MIN);
  for(G4int phi_i=0; phi_i<=num_modules; phi_i++){
	modules_bin.push_back(phi);
	modules_phi.push_back(phi+dphi/2.);
	phi+=dphi;    
  }
  modules_bin.push_back(FLT_MAX);
  modules_phi.push_back(FLT_MAX);
  
  //Rho 
  //layers_rho: first value of rho in each layer
  //layers_type: type of layer (out of 2)
  G4double r=rmin+drho_frontplate;
  layers_bin.push_back(0);
  layers_rho.push_back(0);
  layers_type.push_back(-1);
  for(G4int lay_i=0; lay_i<=num_layers; lay_i++){
    layers_bin.push_back(r);
    layers_rho.push_back(r+drho/2.);
    layers_type.push_back(lay_i%num_layer_types);
	r+=drho;
  }
  layers_bin.push_back(FLT_MAX);
  layers_rho.push_back(FLT_MAX);
  layers_type.push_back(-1);

  //Z: rows_z (position in z) and rows_type (type of tile out of 4)
  for(G4int lay_type=0; lay_type<num_layer_types; lay_type++){
    std::vector<G4float> row_b;
    std::vector<G4float> row_z;
    std::vector<G4int>   row_t;
	z = zmin;
	row_b.push_back(FLT_MIN);
	row_z.push_back(FLT_MIN);
	row_t.push_back(-1);
	for(G4int rep=0; rep<num_reps; rep++){
	  for(G4int tile=0; tile<(G4int)tile_sequence[lay_type].size();tile++){
		G4int tile_type = tile_sequence[lay_type].at(tile);
		row_b.push_back(z);
		row_z.push_back(z+tile_width.at(tile_type)/2.);
		row_t.push_back(tile_type);
		z += tile_width.at(tile_type);
	  }
	}
	//Last is always a master plate
	row_b.push_back(z);
	row_z.push_back(z+tile_width.at(0)/2.);
	row_t.push_back(0);
	//Last is always a master plate
	row_b.push_back(FLT_MAX);
	row_z.push_back(FLT_MAX);
	row_t.push_back(0);
	
	//store in the vectors per layer type
	rows_bin.push_back(row_b);
	rows_z.push_back(row_z);
	rows_type.push_back(row_t);
  }
  
  
  //dump
  if(G4RunManager::GetRunManager()->GetVerboseLevel()>3){
	G4cout << "Modules: " << G4endl;
	for(G4int mod_i=0; mod_i<(G4int)modules_phi.size(); mod_i++){
	  G4cout << modules_phi.at(mod_i) << " ";
	}
	G4cout << G4endl;
	G4cout << "Layers: " << G4endl;
	for(G4int lay_i=0; lay_i<(G4int)layers_rho.size(); lay_i++){
	  G4cout << layers_rho.at(lay_i) << " ";
	}
	G4cout << G4endl;
	for(G4int lay_type=0; lay_type<num_layer_types; lay_type++){
	  G4cout << "Rows Type " << lay_type << ": " << G4endl;
	  for(G4int tile_i=0; tile_i<(G4int)rows_z.at(lay_type).size(); tile_i++){
		G4cout << rows_z.at(lay_type).at(tile_i) << " ";
	  }
	  G4cout << G4endl;
	}
	
  }
}

CaloConcept::~CaloConcept(){

  delete H;
  delete C;
  delete N;
  delete O;
  delete Pb;
  delete W;
  delete Air;
  delete Polystyrene;
  delete Lead;
  delete Steel;
}

void CaloConcept::BuildAndPlace(G4LogicalVolume * world){
  //Create a Calorimeter solid, logical volume and placed volume made out of air
  active_volumes.clear();

  //Set output style
  std::streamsize ss = G4cout.precision();
  G4cout.precision(2);
  G4cout.setf(std::ios::fixed);

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
    G4cout << "Calorimeter volume "
		   << " Rmin: " << GetRmin() / CLHEP::m << " m"
		   << " Rmax: " << (GetRmax()/cos(dphi/2.)) / CLHEP::m << " m"
		   << " Zmin: " << zmin / CLHEP::m << " m"
		   << " Zmax: " << zmax / CLHEP::m << " m"
		   << " HalfZ: " << dz / CLHEP::m << " m"
		   << " PhiMin: " << GetPhiMin()/CLHEP::deg << " deg"
		   << " PhiMax: " << GetPhiMax()/CLHEP::deg << " deg"
		   << G4endl;
  }
  
  G4VSolid * calo_box = new G4Tubs("calo_box",
								   GetRmin(),
								   GetRmax()/cos(dphi/2.),
								   GetHalfZ(),
								   0.*CLHEP::deg /*GetPhiMin()*/,
								   360.*CLHEP::deg /*GetPhiMax()*/);
  G4LogicalVolume * calo_vol = new G4LogicalVolume(calo_box,Air,"calo_vol",0,0,0);
  G4Transform3D tfc          = G4Translate3D(0,0,(GetZmax()+GetZmin())/2.);
  G4VPhysicalVolume * calo_plv = new G4PVPlacement(tfc,calo_vol,"calo_phy",world,false,0);

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
    G4cout << "Placed_Volume " 
		   << " x: "       << std::setw(7) << calo_plv->GetTranslation().x() / CLHEP::mm
		   << " y: "       << std::setw(7) << calo_plv->GetTranslation().y() / CLHEP::mm
		   << " z: "       << std::setw(7) << calo_plv->GetTranslation().z() / CLHEP::mm
		   << " r: "       << std::setw(7) << calo_plv->GetTranslation().r() / CLHEP::mm
		   << " phi: "     << std::setw(7) << calo_plv->GetTranslation().phi() / CLHEP::deg
		   << " overlap: " << std::setw(7) << calo_plv->CheckOverlaps(1000,0.,false)
		   << G4endl;
  }
  

  //G4VisAttributes * calo_volVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
  //calo_volVisAtt->SetForceWireframe(true);
  //calo_vol->SetVisAttributes(calo_volVisAtt);


  std::ostringstream os;
 
  /*
  float tr   = GetRmin();
  float tdr  = 10.*mm;
  float ttn  = tan(GetPhiSize()/2.);
  float tx1  = ttn * (tr-tdr);
  float tx2  = ttn * tr;
  float ty1  = GetHalfZ();
  float ty2  = GetHalfZ();
  float tz   = tdr/2.;

  //Create a pre-sampler, long trapezoidal shape, and volume made out of lead 
  G4Trd * subm_box0 = new G4Trd("subm_box",tx1,tx2,ty1,ty2,tz);
  os.str("");
  os << "subm_vol_presampler";
  G4LogicalVolume * subm_vol0 = new G4LogicalVolume(subm_box0,Lead,os.str());

  //Place it many times in the calorimeter
  double tmod_x_off = tr-tdr/2.;
  double tmod_y_off = 0;  
  for(G4int mod_i=1;mod_i<=(G4int)modules_phi.size()-2;mod_i++){
	double phi = modules_phi.at(mod_i);
	os.str("");
	os << "subm_phy_presampler" << "_" << mod_i;
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Submodule " << os.str() 
			 << " phi: " << (phi)/CLHEP::deg << " deg" 
			 << G4endl;
	}
	
	double tlayer_pos_x = tmod_x_off * cos(phi) - tmod_y_off * sin(phi);
	double tlayer_pos_y = tmod_x_off * sin(phi) + tmod_y_off * cos(phi);
	double tlayer_pos_z = 0;
	G4Transform3D trf = (G4Translate3D(tlayer_pos_x,tlayer_pos_y,tlayer_pos_z)
						 *G4RotateX3D(90*CLHEP::deg)
						 *G4RotateY3D(90*CLHEP::deg)
						 *G4RotateY3D(phi)
						 );
	G4VPhysicalVolume* subm_plv0 = new G4PVPlacement(trf,subm_vol0,os.str(),calo_vol,true,mod_i+1);

	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Placed_Volume "  
			 << " x: "       << std::setw(7) << subm_plv0->GetTranslation().x() / CLHEP::mm
			 << " y: "       << std::setw(7) << subm_plv0->GetTranslation().y() / CLHEP::mm
			 << " z: "       << std::setw(7) << subm_plv0->GetTranslation().z() / CLHEP::mm
			 << " r: "       << std::setw(7) << subm_plv0->GetTranslation().r() / CLHEP::mm
			 << " phi: "     << std::setw(7) << subm_plv0->GetTranslation().phi() / CLHEP::deg
			 << " overlap: " << std::setw(7) << subm_plv0->CheckOverlaps(1000,0.,false) 
			 << G4endl;
	}
	
  }
  */
  
  //Build a long trapezoid (submodule) for each layer to house the tiles
  for(G4int layer_i=1; layer_i<(G4int)layers_rho.size()-2; layer_i++){
	
	G4int layer_type = layers_type.at(layer_i);
	
	//Create a submodule, long trapezoidal shape, and volume made out of air 
	//http://geant4.web.cern.ch/geant4/G4UsersDocuments/UsersGuides/ForApplicationDeveloper/html/Detector/geomSolids.html
	//G4Trd(pName,dx1,dx2,dy1,dy2,dz)
	// dx1 Half-length along x at the surface positioned at -dz
	// dx2 Half-length along x at the surface positioned at +dz
	// dy1 Half-length along y at the surface positioned at -dz
	// dy2 Half-length along y at the surface positioned at +dz
	// dz  Half-length along z axis
 
    float tn   = tan(dphi/2.);
    float dx1  = tn * layers_bin.at(layer_i)  -spacing;
    float dx2  = tn * layers_bin.at(layer_i+1)-spacing;
    float dy1  = dz/2.;
    float dy2  = dz/2.;
    float dz0  = (layers_bin.at(layer_i+1)-layers_bin.at(layer_i))/2.;
   
    if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Create submodule with long trapezoidal shape" << G4endl;
	}
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
	  G4cout << "Trapezoid " 
			 << " dx1:" << std::setw(7) << dx1 
			 << " dx2:" << std::setw(7) << dx2
			 << " dy1:" << std::setw(7) << dy1 
			 << " dy2:" << std::setw(7) << dy2 
			 << " dz0:" << std::setw(7) << dz0 
			 << " LayerType:" << std::setw(7) << layer_type
			 << G4endl;
    }
    
    G4Trd * subm_box = new G4Trd("subm_box",dx1,dx2,dy1,dy2,dz0);
    os.str("");
    os << "subm_v_" << layer_i;
    G4LogicalVolume * subm_vol = new G4LogicalVolume(subm_box,Air,os.str());
	
	
	//Create different types of tiles in the submodule
    if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Create different tiles in layer" << G4endl;
	}
	std::vector<G4LogicalVolume*> tile_vols;
	for(int tile_type=0;tile_type<tile_types;tile_type++){
	  
	  G4Material * material = tile_material.at(tile_type);
	  G4double width = tile_width.at(tile_type);
	  G4bool active = tile_active.at(tile_type);
	  //if(material->GetName()=="Air") continue;
	  
	  dy1 = width/2.;
	  dy2 = width/2.;
	  
	  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
		G4cout << "Trapezoid " 
			   << " dx1:" << std::setw(7) << dx1 
			   << " dx2:" << std::setw(7) << dx2
			   << " dy1:" << std::setw(7) << dy1 
			   << " dy2:" << std::setw(7) << dy2 
			   << " dz0:" << std::setw(7) << dz0 
			   << " Layer: " << std::setw(7) << layer_i
			   << " TileType:" << std::setw(7) <<tile_type
			   << " Active:" << std::setw(7) <<active
			   << " Material:" << std::setw(7) <<material->GetName()
			   << G4endl;
	  }
	  
	  os.str("");
	  os << "tile_b_" << tile_type << "_" << layer_i;
	  G4Trd * tile_b = new G4Trd(os.str(),dx1,dx2,dy1,dy2,dz0);
	  
	  //Create tile volume
	  os.str("");
	  os << "tile_v_" << tile_type << "_" << layer_i;
	  G4LogicalVolume * tile_v = new G4LogicalVolume(tile_b,material,os.str());
	  
	  if(active==true) active_volumes.push_back(tile_v);
	  tile_vols.push_back(tile_v);
	}
	
	//Place all tiles inside the submodule
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Place tiles in submodule " << G4endl;
	}
	
	std::vector<int> row_copies(5,0);
	for(G4int row_i=1;row_i<=(G4int)rows_z.at(layer_type).size()-2;row_i++){
	  G4int tile_type = rows_type.at(layer_type).at(row_i);
	  G4double row_z = rows_z.at(layer_type).at(row_i)-(zmax+zmin)/2.;
	  row_copies.at(tile_type)++;
	  os.str("");
	  os << "tile_p_" << layer_i << "_" << row_i << "_" << tile_type;
	  if(tile_active.at(tile_type)==true) os << "_a";
	  else{ os << "_p"; }
	  G4Transform3D tft = (G4Translate3D(0,row_z,0));
	  G4VPhysicalVolume * tile_plv = new G4PVPlacement(tft,tile_vols.at(tile_type),os.str(),
													   subm_vol,true,row_copies.at(tile_type));
	  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
		G4cout << "Placed_Volume " 
			   << " x: "       << std::setw(7) << tile_plv->GetTranslation().x() / CLHEP::mm
			   << " y: "       << std::setw(7) << tile_plv->GetTranslation().y() / CLHEP::mm
			   << " z: "       << std::setw(7) << tile_plv->GetTranslation().z() / CLHEP::mm
			   << " overlap: " << std::setw(2) << tile_plv->CheckOverlaps(1000,0.,false) 
			   << " row: "     << std::setw(7) << row_i 
			   << " TileType: "<< std::setw(2) << tile_type 
			   << G4endl;
	  }

	}
	
	
    //Place the same submodule many times in the calorimeter
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Place submodule in calo volume " << G4endl;
	}
    double mod_x_off = layers_rho.at(layer_i);  
    double mod_y_off = 0;  
    for(G4int mod_i=1;mod_i<(G4int)modules_phi.size()-2;mod_i++){
      double phi = modules_phi.at(mod_i);
      os.str("");
	  os << "subm_phy_" << mod_i << "_" << layer_i;      
      
      double layer_pos_x = mod_x_off * cos(phi) - mod_y_off * sin(phi);
      double layer_pos_y = mod_x_off * sin(phi) + mod_y_off * cos(phi);
      double layer_pos_z = 0;
      G4Transform3D trf = (G4Translate3D(layer_pos_x,layer_pos_y,layer_pos_z)
						   *G4RotateX3D(90*CLHEP::deg)
						   *G4RotateY3D(90*CLHEP::deg)
						   *G4RotateY3D(phi)
						   );
	  G4VPhysicalVolume * subm_plv = new G4PVPlacement(trf,subm_vol,os.str(),calo_vol,true,mod_i);
	  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
		G4cout << "Placed_Volume "
			   << " x: "       << std::setw(7) << subm_plv->GetTranslation().x() / CLHEP::mm
			   << " y: "       << std::setw(7) << subm_plv->GetTranslation().y() / CLHEP::mm
			   << " z: "       << std::setw(7) << subm_plv->GetTranslation().z() / CLHEP::mm
			   << " r: "       << std::setw(7) << subm_plv->GetTranslation().r() / CLHEP::mm
			   << " phi: "     << std::setw(7) << subm_plv->GetTranslation().phi() / CLHEP::deg
			   << " overlap: " << std::setw(7) << subm_plv->CheckOverlaps(1000,0.,false) 
			   << G4endl;
	  }

    }
	
  }

  //Clear output defaults
  G4cout.precision(ss);
  G4cout.unsetf(std::ios::fixed);
  
}


G4int CaloConcept::GetCellId(const G4Step * aStep){
  
  G4ThreeVector pre = aStep->GetPreStepPoint()->GetPosition();  
  G4ThreeVector pos = aStep->GetPostStepPoint()->GetPosition();  
  
  G4bool preatborder = (aStep->GetPreStepPoint()->GetStepStatus() == fGeomBoundary);
  G4bool posatborder = (aStep->GetPostStepPoint()->GetStepStatus() == fGeomBoundary);
  
  //Using lower_bound which finds the first item that is not less than value
  //Because the bins are in ROOT histogram convention where
  //each bin value represents the lower bound of the bin
  //if the found item is not equal to the value, index of bin is changed to item-1

  std::vector<G4float>::iterator low;
  G4float difpre0, difpre1, difpre2;
  G4float difpos0, difpos1, difpos2;
  G4float difpospre;

  std::ostringstream os;
  if(preatborder){ os << "PosAtBorder "; }
  if(posatborder){ os << "NexAtBorder "; }
  
  //Find module in phi
  low=std::lower_bound (modules_bin.begin(), modules_bin.end(), pos.phi());
  low--;
  G4int mod = (low-modules_bin.begin());
  difpre1 = pre.phi()-(*low);
  difpos1 = pos.phi()-(*low);
  os << " PhiDifPre1: " << difpre1 << " PhiDifPos1: " << difpos1 << " "; 

  //Find layer in rho
  low=std::lower_bound (layers_bin.begin(), layers_bin.end(), pos.rho());
  low--;
  G4int lay = (low-layers_bin.begin());
  difpre1 = pre.rho()-(*low);
  difpos1 = pos.rho()-(*low);
  os << " RhoDifPre1: " << difpre1 << " RhoDifPos1: " << difpos1 << " "; 
  
  //Find row in z
  G4int typ = layers_type.at(lay); 
  low=std::lower_bound (rows_bin.at(typ).begin(), rows_bin.at(typ).end(), pos.z());//,cmpfunc);
  difpospre = pos.z()-pre.z();
  difpre0 = (*(low-1))-pre.z();
  difpos0 = (*(low-1))-pos.z();
  difpre1 = (*low)-pre.z();
  difpos1 = (*low)-pos.z();
  difpre2 = (*(low+1))-pre.z();
  difpos2 = (*(low+1))-pos.z();
  os << " ZDifPosPre: " << difpospre 
	 << " ZDifPre0: " << difpre0 
	 << " ZDifPre1: " << difpre1
	 << " ZDifPre2: " << difpre2
	 << " ZDifPos0: " << difpos0 
	 << " ZDifPos1: " << difpos1
	 << " ZDifPos2: " << difpos2
	 << " "; 
  if     (fabs(difpre0)<epsilon && fabs(difpos0)<epsilon && difpre1>0 && difpos1>0){os << "CaseA ";low-=2;}
  else if(difpre0>0             && fabs(difpos0)<epsilon && difpre1>0 && difpos1>0){os << "CaseB ";low-=2;}
  else if(fabs(difpre0)<epsilon && difpos0>0             && difpre1>0 && difpos1>0){os << "CaseC ";low-=2;}
  else if(fabs(difpre1)<epsilon && fabs(difpos1)<epsilon && difpre2>0 && difpos2>0){os << "Case0 ";}
  else if(difpre1>0             && difpos1>0             && difpre2>0 && difpos2>0){os << "Case1 ";low--;}
  else if(fabs(difpre1)<epsilon && difpos1>0             && difpre2>0 && difpos2>0){os << "Case2 ";low--;}
  else if(difpre1>0             && fabs(difpos1)<epsilon && difpre2>0 && difpos2>0){os << "Case3 ";low--;}
  else if(difpre1<0             && difpos1<0             && difpre2>0 && difpos2>0){os << "Case4 ";}
  else if(fabs(difpre1)<epsilon && difpos1<0             && difpre2>0 && difpos2>0){os << "Case5 ";}
  else if(difpre1<0             && fabs(difpos1)<epsilon && difpre2>0 && difpos2>0){os << "Case6 ";}

  G4int row = (low-rows_bin.at(typ).begin());
  
  
  G4int tile_type = rows_type.at(typ).at(row);
  G4bool is_active = tile_active.at(tile_type);
  
  G4int cid = mod*100*10000+lay*10000+row;

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
    std::streamsize ss = G4cout.precision();
	G4cout.precision(4);
	G4cout.setf(std::ios::fixed);
	G4cout << "CaloConcept::GetCellId" << " " 
		   << "cellid: "  << std::setw(7) << cid << " "
		   << "mod: "     << std::setw(3) << mod << " "
		   << "lay: "     << std::setw(3) << lay << " "
		   << "row: "     << std::setw(5) << row << " "
		   << "active: "  << std::setw(1) << is_active << " " 
		   << "type: "    << std::setw(1) << tile_type << " "
		   << "msg: "     << os.str()
		   << G4endl
		   << "low  "  
		   << "phi: "     << std::setw(7) << modules_bin.at(mod)      << " "
		   << "rho: "     << std::setw(8) << layers_bin.at(lay)       << " "
		   << "z: "       << std::setw(8) << rows_bin.at(typ).at(row) << " " 
		   << G4endl
		   << "hit  "
		   << "phi: "     << std::setw(7) << pre.phi() << " "
		   << "rho: "     << std::setw(8) << pre.rho() << " "
		   << "z: "       << std::setw(8) << pre.z()   << " " 
		   << "x: "       << std::setw(8) << pre.x()   << " " 
		   << "y: "       << std::setw(8) << pre.y()   << " " 
		   << "m: "       << aStep->GetPreStepPoint()->GetMaterial()->GetName() << " "
		   << G4endl
		   << "hitn "
		   << "phi: "     << std::setw(7) << pos.phi() << " "
		   << "rho: "     << std::setw(8) << pos.rho() << " "
		   << "z: "       << std::setw(8) << pos.z()   << " " 
		   << "x: "       << std::setw(8) << pos.x()   << " " 
		   << "y: "       << std::setw(8) << pos.y()   << " " 
		   << "m: "       << aStep->GetPostStepPoint()->GetMaterial()->GetName() << " "
		   << G4endl
		   << "cell " 
		   << "phi: "     << std::setw(7) << modules_phi.at(mod)    << " "
		   << "rho: "     << std::setw(8) << layers_rho.at(lay)     << " "
		   << "z: "       << std::setw(8) << rows_z.at(typ).at(row) << " " 
		   << G4endl
		   << "high "
		   << "phi: "     << std::setw(7) << modules_bin.at(mod+1)      << " "
		   << "rho: "     << std::setw(8) << layers_bin.at(lay+1)       << " "
		   << "z: "       << std::setw(8) << rows_bin.at(typ).at(row+1) << " " 
		   << G4endl;
	std::cout.precision(ss);
  }
  
  return cid;
}

G4double CaloConcept::GetCellPhi(G4int cellid){
  G4int mod = (G4int)cellid/(100*10000);
  //if(mod<0||mod>modules_phi.size()-1) return -999.;
  return modules_phi.at(mod);
}

G4double CaloConcept::GetCellRho(G4int cellid){
  G4int lay = (G4int)(cellid/10000) % 100;
  //if(lay<0||lay>layers_r.size()-1||lay>layers_h.size()-1) return -999.;
  return layers_rho.at(lay);
}

G4double CaloConcept::GetCellZ(G4int cellid){
  G4int lay = (G4int)(cellid/10000) % 100;
  G4int row = (G4int)(cellid%10000);
  G4int typ = layers_type.at(lay);
  return rows_z.at(typ).at(row);
}

G4double CaloConcept::GetCellDeltaPhi(G4int /*cellid*/){
  return dphi;
}

G4double CaloConcept::GetCellDeltaRho(G4int /*cellid*/){
  return drho;
}

G4double CaloConcept::GetCellDeltaZ(G4int cellid){
  G4int lay = (G4int)(cellid/10000) % 100;
  G4int row = (G4int)(cellid%10000);
  G4int typ = layers_type.at(lay);
  return tile_width.at(rows_type.at(typ).at(row));
}

G4double CaloConcept::GetRmin(){
  return rmin;
}

G4double CaloConcept::GetRmax(){
  return rmax;
}

G4double CaloConcept::GetPhiMin(){
  return phimin;
}

G4double CaloConcept::GetPhiMax(){
  return phimax;
}

G4double CaloConcept::GetZmax(){
  return zmax;
}

G4double CaloConcept::GetZmin(){
  return zmin;
}

G4double CaloConcept::GetHalfZ(){
  return dz/2.;
}

G4double CaloConcept::GetDz(){
  return dz;
}

G4int CaloConcept::GetNumPhiBins(){
  return num_modules;
}

G4double CaloConcept::GetPhiBin(G4int bin){
  return modules_phi.at(bin)-dphi/2.;
}

G4double CaloConcept::GetLayerHeight(G4int /*layer*/){
  return drho;
}

G4int CaloConcept::GetNumTileTypes(){
  return tile_types;
}

G4double CaloConcept::GetPhiSize(){
  return dphi;
}

G4int CaloConcept::GetNumLayers(){
  return num_layers;
}

G4int CaloConcept::GetNumRows(G4int layer){
  return rows_z.at(layers_type.at(layer)).size();
}

G4double CaloConcept::GetRowWidth(G4int row, G4int layer){
  return rows_z.at(layers_type.at(layer)).at(row);
}

G4double CaloConcept::GetRowZ(G4int row, G4int layer){
  G4int typ = layers_type.at(layer);
  return rows_z.at(typ).at(row)-tile_width.at(rows_type.at(typ).at(row))/2.;
}

G4int CaloConcept::GetNumCells(){
  return 129*100*10000;
}

std::vector<G4LogicalVolume*> CaloConcept::GetActiveVolumes(){
  return active_volumes;
}

G4double CaloConcept::BirkLaw(const G4Step * aStep) const {
  // *** apply BIRK's saturation law to energy deposition ***
  // *** only organic scintillators implemented in this version MODEL=1
  //
  // Note : the material is assumed ideal, which means that impurities
  //        and aging effects are not taken into account
  //
  // algorithm : edep = destep / (1. + RKB*dedx + C*(dedx)**2)
  //
  // the basic units of the coefficient are g/(MeV*cm**2)
  // and de/dx is obtained in MeV/(g/cm**2)
  //
  // exp. values from NIM 80 (1970) 239-244 :
  //
  // RKB = 0.013  g/(MeV*cm**2)  and  C = 9.6e-6  g**2/((MeV**2)(cm**4))
  
  const G4String myMaterial = "Polystyrene";
  const G4double birk1 = 0.0130*CLHEP::g/(CLHEP::MeV*CLHEP::cm2);
  const G4double birk2 = 9.6e-6*CLHEP::g/(CLHEP::MeV*CLHEP::cm2)*CLHEP::g/(CLHEP::MeV*CLHEP::cm2);
  G4double response = 0.;
  
  G4double destep      = aStep->GetTotalEnergyDeposit();
  //  doesn't work with shower parameterisation
  //  G4Material* material = aStep->GetTrack()->GetMaterial();
  //  G4double charge      = aStep->GetTrack()->GetDefinition()->GetPDGCharge();
  G4Material* material = aStep->GetPreStepPoint()->GetMaterial();
  G4double charge      = aStep->GetPreStepPoint()->GetCharge();
  
  // --- no saturation law for neutral particles ---
  // ---  and materials other than scintillator  ---
  if ( (charge!=0.) && (material->GetName()==myMaterial) ) {
	G4double rkb = birk1;
	// --- correction for particles with more than 1 charge unit ---
	// --- based on alpha particle data (only apply for MODEL=1) ---
	if (fabs(charge) > 1.0) rkb *= 7.2/12.6;
	
	if(aStep->GetStepLength() != 0 ) {
	  G4double dedx = destep/(aStep->GetStepLength())/(material->GetDensity());
	  response = destep/(1. + rkb*dedx + birk2*dedx*dedx);
	}else{
	  response = destep;
	}
	return response;
  } else {
	return destep;
  }
}
