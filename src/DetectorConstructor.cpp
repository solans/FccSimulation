/************************************************
 * DetectorConstructor.cpp
 * Geant4 detector constructor
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#include "DetectorConstructor.h"
#include "SensitiveDetector.h"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include <sstream>
#include <vector>
#include "CaloConcept.h"

DetectorConstructor::DetectorConstructor()
{
  // experimental hall
  
  expHall_x = 50.*m;
  expHall_y = 50.*m;
  expHall_z = 50.*m;

  // Materials

  G4double a, z;
  
  N  = new G4Element("Nitrogen", "N",  z=7.,  a= 14.01*g/mole);
  O  = new G4Element("Oxygen",   "O",  z=8.,  a= 16.00*g/mole);  
  
  //Air = new G4Material("Air", density=1.29*mg/cm3, nel=2);
  //Air->AddElement(N, 70.*perCent);
  //Air->AddElement(O, 30.*perCent);
  
  //Vacuum
  Air = new G4Material("Galactic", z=1., a=1.01*g/mole, CLHEP::universe_mean_density,
					   kStateGas,0.1*kelvin,1.e-19*pascal);
  // Calorimeter description
  caloConcept = new CaloConcept();

  //Shrink the world to the calorimeter size
  expHall_x = expHall_y = 2.*caloConcept->GetRmax()+1.*m;
  expHall_z = 2.*caloConcept->GetDz()+1.*m;
  
}

DetectorConstructor::~DetectorConstructor()
{
  delete N;
  delete O;
  delete Air;

  delete caloConcept;
}

G4VPhysicalVolume* DetectorConstructor::Construct()
{

  // Experimental hall
  G4Box * world_box = new G4Box("expHall_b",expHall_x,expHall_y,expHall_z);
  G4LogicalVolume * world_vol = new G4LogicalVolume(world_box,Air,"expHall_v",0,0,0);  
  G4VPhysicalVolume * world_phy = new G4PVPlacement(0,G4ThreeVector(),world_vol,"expHall_p",0,false,0);
  world_vol->SetVisAttributes(G4VisAttributes::GetInvisible());

  //Create the Calorimeter detector and place it in the world
  caloConcept->BuildAndPlace(world_vol);
  
#if G4VERSION_NUMBER < 1000
  
  //Create the sensitive detector and register it in the SDmanager
  //Geant4.9.6
  //calo_vol->SetSensitiveDetector(calo_sd);
  
  G4VSensitiveDetector * calo_sd = new SensitiveDetector("/Example/Calorimeter", caloConcept);
  ((G4SDManager *) G4SDManager::GetSDMpointer())->AddNewDetector(calo_sd);
  for(unsigned int i=0;i<caloConcept->GetActiveVolumes().size();i++){
    caloConcept->GetActiveVolumes().at(i)->SetSensitiveDetector(calo_sd);
  }
  
#endif
 
  // Return the placed volume of the experimental hall

  return world_phy;

}

#if G4VERSION_NUMBER >= 1000

void DetectorConstructor::ConstructSDandField(){
  
  //Create the sensitive detector and register it in the SDmanager
  //Geant4.10.0
  //SetSensitiveDetector("calo_vol",calo_sd);
  
  G4VSensitiveDetector * calo_sd = new SensitiveDetector("/Example/Calorimeter", caloConcept);
  ((G4SDManager *) G4SDManager::GetSDMpointer())->AddNewDetector(calo_sd);
  
  for(unsigned int i=0;i<caloConcept->GetActiveVolumes().size();i++){
    SetSensitiveDetector(caloConcept->GetActiveVolumes().at(i)->GetName(),calo_sd);
  }
} 

#endif
