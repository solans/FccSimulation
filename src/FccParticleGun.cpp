/*************************************************
 * Particle Gun MC simulation
 * Carlos.Solans@cern.ch
 * August 2014
 *
 * This is based on Geant 4 software
 *************************************************/

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "DetectorConstructor.h"
#include "ParticleGun.h"
#include "RunAction.h"
#include "StackingAction.h"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "globals.hh"
#include "G4Timer.hh"
#include "FTFP_BERT.hh"
#include "QGSP_BERT.hh"

#include <cmdl/cmdargs.h>

int main(int argc, const char** argv){
  
  //Parse arguments
  CmdArgStr ofile('o', "ofile", "file", "output file (output.root)");
  CmdArgStr particle('p', "particle", "particle", "particle description", CmdArg::isREQ);
  CmdArgInt nevents ('n', "nevents", "events", "number of events (1)");
  CmdArgInt verbose ('v', "verbose", "level", "verbose level (0)");
  CmdArgBool viewer ('w', "viewer", "enable viewer file");
  CmdArgFloat energy('e', "energy", "energy", "energy in GeV", CmdArg::isREQ);
  CmdArgFloat angle('a', "angle", "degree", "incident angle");
  CmdArgStr plist('l', "list", "physics-list", "physics list (FTFP_BERT)");

  CmdLine cmd(*argv,
			  & nevents,
			  & ofile,
			  & verbose,
			  & viewer,
			  & angle,
			  & particle,
			  & energy,
			  & plist,
			  NULL);

  nevents = 1;
  verbose = 0;
  angle = 0;
  ofile = "output.root";
  
  CmdArgvIter argv_iter(--argc, ++argv);
  cmd.parse(argv_iter);

  //Random seeds
  //https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch03s02.html
  // Set the random seeds to a random value to get independent events
  // Seems the default behaviour in version 10 is to repeat the same events
  // http://hypernews.slac.stanford.edu/HyperNews/geant4/get/runmanage/371/1/1/1.html
  
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  
  //Todo list 
  //Particle cut
  //http://geant4.cern.ch/G4UsersDocuments/UsersGuides/ForApplicationDeveloper/html/TrackingAndPhysics/thresholdVScut.html
  
  G4Timer * timer = new G4Timer();
  timer->Start();

  // Create the run manager
  G4RunManager* runManager = new G4RunManager();
  runManager->SetVerboseLevel(verbose);
  
  // Construct a detector and pass it to the run manager.
  // DetectorConstructor builds the detector geometry and returns the top placed volume.
  // It contains the sensitive detector that processes the hits,
  // and the read-out geometry.
  G4VUserDetectorConstruction* detector = new DetectorConstructor();
  runManager->SetUserInitialization(detector);
  
  // Intialize the physics lists and pass them to run manager
  G4VModularPhysicsList* physics = 0;
  if(G4String(plist)==G4String("QGSP_BERT")){ physics = new QGSP_BERT(); }
  else                                      { physics = new FTFP_BERT(); }
  //physics->RemovePhysics("neutronTrackingCut");
  //G4NeutronTrackingCut* neutrontracking=new G4NeutronTrackingCut(1);
  //neutrontracking->SetTimeLimit(100.*ns);
  //physics->RegisterPhysics(neutrontracking);
  runManager->SetUserInitialization(physics);

  // Initialize the run manager
  runManager->Initialize();

  // Define the generator for primary vertices and pass it to run manager
  ParticleGun* particleGun = new ParticleGun();
  runManager->SetUserAction(particleGun);
  
  //Define action for beginning and end of every run
  G4UserRunAction* run_action = new RunAction(G4String(ofile));
  runManager->SetUserAction(run_action);

  //Don't track neutrinos
  G4UserStackingAction * stack_action = new StackingAction();
  runManager->SetUserAction(stack_action);

  if(viewer){
    G4VisManager* visManager = new G4VisExecutive();
    visManager->Initialize();
    
    G4UImanager* ui = G4UImanager::GetUIpointer();
    ui->ApplyCommand("/vis/create");
    ui->ApplyCommand("/vis/open HepRepFile");
    ui->ApplyCommand("/vis/drawVolume");
    ui->ApplyCommand("/vis/viewer/update");
    ui->ApplyCommand("/vis/viewer/refresh");
    ui->ApplyCommand("/vis/scene/add/axes 0 0 0 1 m");
    ui->ApplyCommand("/vis/scene/add/hits");
    //ui->ApplyCommand("/vis/scene/add/trajectories");
    ui->ApplyCommand("/vis/viewer/flush");
  }

  //Configure the particle gun
  //Theta is the angle measured from the z axis up to the vector
  G4ThreeVector direction, position;
  direction.setRhoPhiTheta(1.,5.*CLHEP::deg,(90.-angle)*CLHEP::deg);
  //position.setRhoPhiTheta(1.,0.,(90.-angle)*CLHEP::deg);
  particleGun->Configure(particle,energy,direction);

  if(verbose>2){
	G4UImanager* UI = G4UImanager::GetUIpointer();
	UI->ApplyCommand("/tracking/verbose 2");
  }

  //Run the events
  runManager->BeamOn(nevents);

  // Delete run manager. 
  // This deletes actions, physics lists and detectors passed to it
  delete runManager;

  timer->Stop();
  G4cout << "Simulation time: " << timer->GetRealElapsed() << " s " << G4endl;
  G4cout << "Have a nice day" << G4endl;

  delete timer;
  return 0;
}
