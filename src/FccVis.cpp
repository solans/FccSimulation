/*************************************************
 * Visualization tool
 * Carlos.Solans@cern.ch
 * August 2014
 *
 * This is based on Geant4 software
 *************************************************/

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIsession.hh"
#include "G4UIterminal.hh"


#include "DetectorConstructor.h"
#include "QGSP_BERT.hh"
#include "ParticleGun.h"
#include "RunAction.h"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"

#include <cmdl/cmdargs.h>

using namespace std;

int main(int argc, const char** argv){
  
  //Parse arguments
  CmdArgStr particle('p', "particle", "particle", "particle description");
  CmdArgFloat energy('e', "energy", "energy", "energy in GeV");

  CmdLine cmd(*argv,
	      & particle,
	      & energy,
	      NULL);
  
  CmdArgvIter argv_iter(--argc, ++argv);
  cmd.parse(argv_iter);
 
  // Create the run manager
  G4RunManager* runManager = new G4RunManager();
  
  // Construct a detector and pass it to the run manager.
  // DetectorConstructor builds the detector geometry and returns the top placed volume.
  // It contains the sensitive detector that processes the hits,
  // and the read-out geometry.
  G4VUserDetectorConstruction* detector = new DetectorConstructor();
  runManager->SetUserInitialization(detector);
  
  // Intialize the physics lists and pass them to run manager
  G4VUserPhysicsList* physics = new QGSP_BERT();
  runManager->SetUserInitialization(physics);

  // Initialize the run manager
  runManager->Initialize();

  // Define the generator for primary vertices and pass it to run manager
  //PrimaryGenerator* gen_action = new PrimaryGenerator();
  //runManager->SetUserAction(gen_action);
  
  //Define action for beginning and end of every run
  G4UserRunAction* run_action = new RunAction();
  runManager->SetUserAction(run_action);
    
  G4VisManager* visManager = new G4VisExecutive();
  visManager->Initialize();
  
  //  G4UIsession * ses = new G4UIterminal();
  //ses->SessionStart();

  G4UImanager* ui = G4UImanager::GetUIpointer();
  ui->ApplyCommand("/vis/create");
  ui->ApplyCommand("/vis/open HepRepFile");
  ui->ApplyCommand("/vis/drawVolume");
  ui->ApplyCommand("/vis/viewer/update");
  ui->ApplyCommand("/vis/viewer/flush");
  
  //delete ses;

  //Configure the particle gun
  //gen_action->Configure(particle,energy);
  
  //runManager->BeamOn(0);

  // Delete run manager. 
  // This deletes actions, physics lists and detectors passed to it
  delete runManager;

  return 0;
}
