/************************************************
 * ParticleGun.cpp
 * Geant4 event generator
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#include "ParticleGun.h"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
//#include "G4ParticleDefinition.hh"

ParticleGun::ParticleGun(){
  G4cout << "ParticleGun::ParticleGun" << G4endl; 
  particleGun = new G4ParticleGun(1/*number of particles*/); 
  particleGun->SetParticleDefinition(G4ParticleTable::GetParticleTable()->FindParticle("pi+"));
  particleGun->SetParticlePosition(G4ThreeVector(0.*cm,0.*cm,0.*cm));  
  particleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.05,0.05));
  particleGun->SetParticleEnergy(1000.*GeV);
}

ParticleGun::~ParticleGun(){
  G4cout << "ParticleGun::~ParticleGun" << G4endl;
  delete particleGun;
}

void ParticleGun::GeneratePrimaries(G4Event* event){
  G4cout << "ParticleGun::GeneratePrimaries" << G4endl;
  particleGun->GeneratePrimaryVertex(event);
}

void ParticleGun::Configure(const char * particle, float energyGev, const G4ThreeVector & dir){
  particleGun->SetParticleDefinition(G4ParticleTable::GetParticleTable()->FindParticle(particle));
  //particleGun->SetParticlePosition(G4ThreeVector(1.*m,0.*m,12*mm));  
  particleGun->SetParticleEnergy(energyGev*GeV);
  particleGun->SetParticleMomentumDirection(dir);
}

