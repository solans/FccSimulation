#include "RunAction.h"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "g4root.hh"
#include "Randomize.hh"
#include <ctime>

RunAction::RunAction(G4String outputfile){
  outfile = outputfile;
}

RunAction::~RunAction(){}

void RunAction::BeginOfRunAction(const G4Run* aRun){
  
  G4cout << "RunAction::BeginOfRunAction " << aRun->GetRunID() << G4endl;

  // Set the random seeds to a random value to get independent events
  // Seems the default behaviour in version 10 is to repeat the same events
  // http://hypernews.slac.stanford.edu/HyperNews/geant4/get/runmanage/371/1/1/1.html
  long seeds[2];
  time_t systime = time(NULL);
  seeds[0] = (long) systime;
  seeds[1] = (long) (systime*G4UniformRand());
  G4Random::setTheSeeds(seeds);  

  // Get analysis manager pointer
  G4AnalysisManager* man = G4AnalysisManager::Instance();
  // Create an ntuple
  man->CreateNtuple("Hits", "Simple hits");
  man->CreateNtupleIColumn("Event");
  man->CreateNtupleDColumn("Energy");
  man->CreateNtupleDColumn("Rho");
  man->CreateNtupleDColumn("Phi");
  man->CreateNtupleDColumn("Z");
  man->CreateNtupleIColumn("Id");
  man->FinishNtuple();
  man->OpenFile(outfile);

  //G4RunManager::GetRunManager()->SetRandomNumberStore(true);
}

void RunAction::EndOfRunAction(const G4Run* aRun){
  
  G4cout << "RunAction::EndOfRunAction " << aRun->GetRunID() << G4endl;
  G4AnalysisManager* man = G4AnalysisManager::Instance();
  man->Write();
  man->CloseFile();  
  delete man;
}
