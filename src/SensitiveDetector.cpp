/************************************************
 * SensitiveDetector.h
 * Geant4 sensitive detector
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#include "SensitiveDetector.h"

#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4ParticleDefinition.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4VAnalysisManager.hh"
#include "G4SystemOfUnits.hh"
#include "g4root.hh"
#include "CaloConcept.h"
#include <iostream>
#include <exception>

SensitiveDetector::SensitiveDetector(G4String name,CaloConcept * cc):
  G4VSensitiveDetector(name),
  hitsCollection(0),
  caloConcept(cc)
{
  collectionName.insert("SimpleHits");
}

SensitiveDetector::~SensitiveDetector(){}

void SensitiveDetector::Initialize(G4HCofThisEvent* hce){
  G4int evnum = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4cout << "SensitiveDetector::Initialize " << evnum <<G4endl;
  
  //Create new collection of hits
  hitsCollection = new SimpleHitsCollection(SensitiveDetectorName,collectionName[0]); 
  //Add this collection to the hits collection of this event
  G4int collectionID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  //Register the collection in the hit collections of this event
  hce->AddHitsCollection(collectionID,hitsCollection);

  //Initialize the hits
  hitsCollection->GetVector()->resize(caloConcept->GetNumCells(),NULL);
}

G4bool SensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*){

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
    G4cout << "SensitiveDetector::ProcessHits" << G4endl;
  }

  //G4double edep = aStep->GetTotalEnergyDeposit();
  G4double edep = caloConcept->BirkLaw(aStep);
  G4String part = aStep->GetTrack()->GetDefinition()->GetParticleType();
  
  if(edep<=0 && part !="geantino") {
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
	  G4cout << "Abort because E<=0" << G4endl
			 << "SensitiveDetector::ProcessHits" << G4endl;
	}
	return true;
  }else{
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
	  G4cout << "Step Energy deposit (MeV) = " << edep/MeV << G4endl;
	}
  }
	
  try{

	G4int cellid=caloConcept->GetCellId(aStep);
	
	if(cellid<0) {
	  if(G4RunManager::GetRunManager()->GetVerboseLevel()>-1){
		G4cout << "Abort because cellid<0" << G4endl
			   << "SensitiveDetector::ProcessHits" << G4endl;
	  }
	  return false;
	}
	
	G4double cellphi = caloConcept->GetCellPhi(cellid);
	G4double cellrho = caloConcept->GetCellRho(cellid);
	G4double cellz   = caloConcept->GetCellZ(cellid);
    	
	
	SimpleHit * hit = (SimpleHit*)hitsCollection->GetHit(cellid);
	if(hit==NULL){
	  G4ThreeVector cellpos;
	  cellpos.setRhoPhiZ(cellrho,cellphi,cellz);
	  hit = new SimpleHit();
	  hit->SetId(cellid);
	  hit->SetPosition(cellpos);
	  hit->SetEnergy(edep);
	  hitsCollection->GetVector()->at(cellid)=hit;
	}else{
	  hit->AddEnergy(edep);
	}
	
	
  }catch(std::exception &e){
	if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
      G4cout << "Abort due to exception" << G4endl;
    }
    return false;
  }
  
  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
	G4cout << "SensitiveDetector::ProcessHits" << G4endl;
  }
  
  return true;
}

void SensitiveDetector::EndOfEvent(G4HCofThisEvent*){
  G4int evnum = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4cout << "SensitiveDetector::EndOfEvent " << evnum << G4endl;
  
  G4VAnalysisManager* man = G4AnalysisManager::Instance();
  
  G4double totE = 0;
  G4double maxR = 0;
  G4double maxE = 0;
  G4double maxE_R = 0;
  G4int    totC = 0;

  for(G4int i=0;i<hitsCollection->entries();i++){
    SimpleHit * hit = (SimpleHit*)hitsCollection->GetHit(i); 
    if(hit==NULL){continue;}
    if(hit->GetEnergy()==0.){continue;}
    //if(hit->GetEnergy()<1.*eV){continue;}
    if(hit->GetEnergy()<1.*keV){continue;}
    //Fill ntuple
	man->FillNtupleIColumn(0,evnum);
    man->FillNtupleDColumn(1,hit->GetEnergy());         //MeV
    man->FillNtupleDColumn(2,hit->GetPosition().rho()); //mm
    man->FillNtupleDColumn(3,hit->GetPosition().phi()); //rad
    man->FillNtupleDColumn(4,hit->GetPosition().z());   //mm
    man->FillNtupleIColumn(5,hit->GetId());
    man->AddNtupleRow();
    //Event statistics
	totC++;
    totE += hit->GetEnergy(); 
    G4double rho = hit->GetPosition().rho();
    if(rho>maxR)             { maxR = rho; }
    if(hit->GetEnergy()>maxE){ maxE = hit->GetEnergy(); maxE_R=rho; } 
  }

  G4cout.precision(2);
  G4cout.setf(std::ios::fixed);
  G4cout << "     Num cells hit :                  " << totC << G4endl
		 << "     E deposited in the calorimeter : " << totE / GeV << " GeV" << G4endl
         << "     Max penetration :                " << maxR / m   << " m" << G4endl
         << "     Max Energy deposit at :          " << maxE_R / m << " m" << G4endl;

  G4cout << "SensitiveDetector::EndOfEvent " << evnum << G4endl;
  
}


/*
void SensitiveDetector::clear(){} 

void SensitiveDetector::DrawAll(){}

void SensitiveDetector::PrintAll(){}
*/
