/************************************************
 * SimpleHit.cpp
 * Geant4 Hit
 * author: Carlos.Solans@cern.ch
 * date: November 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#include "SimpleHit.h"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"

G4Allocator<SimpleHit> SimpleHitAllocator;

SimpleHit::SimpleHit(){
  m_e=0;
  m_id=0;
}

SimpleHit::~SimpleHit(){}

void SimpleHit::AddEnergy(G4double e){m_e+=e;}

const G4ThreeVector & SimpleHit::GetPosition(){
  //return G4ThreeVector(m_p1,m_p2,m_p3);
  return m_pos;
}

G4double SimpleHit::GetEnergy(){return m_e;}

G4int SimpleHit::GetId(){return m_id;}

void SimpleHit::SetEnergy(G4double e){m_e = e;}

void SimpleHit::SetPosition(const G4ThreeVector &pos){
  m_pos=pos;
}

void SimpleHit::SetId(G4int id){m_id = id;}

void SimpleHit::Draw(){
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager==NULL) return;

  // define a circle in a 3D space
  G4Circle circle(m_pos);
  circle.SetScreenSize(0.01*m_e/CLHEP::GeV);
  circle.SetFillStyle(G4Circle::filled);
  
  // make the circle red
  G4Colour colour(1.,0.,0.);
  G4VisAttributes attribs(colour);
  circle.SetVisAttributes(attribs);
    
  // make a 3D data for visualization (Step2 of the Clear-Draw-Show process)
  pVVisManager->Draw(circle);
}

void SimpleHit::Print(){}
