#include "StackingAction.h"
#include "G4Track.hh"
#include "G4NeutrinoE.hh"

StackingAction::StackingAction(){}

StackingAction::~StackingAction(){}

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track* track){
  //kill secondary neutrino
  if (track->GetParentID()>0 && track->GetDefinition() == G4NeutrinoE::NeutrinoE()){
	return fKill;
  //otherwise, return what Geant4 would have returned by itself
  }else{
	return G4UserStackingAction::ClassifyNewTrack(track);
  }
}
